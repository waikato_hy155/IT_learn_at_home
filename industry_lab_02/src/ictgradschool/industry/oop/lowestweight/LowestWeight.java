package ictgradschool.industry.oop.lowestweight;

import ictgradschool.Keyboard;
import org.w3c.dom.ls.LSOutput;

import java.sql.SQLOutput;

/**
 * Write a program which asks the user to enter the weights of two people,
 * then uses Math.min() to determine the lowest weight, and prints out the result.
 */
public class LowestWeight {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private void start() {

        double weight1 = getWeight();
        double weight2 = getWeight();
        double lowest = Math.min(weight1,weight2);
        System.out.println("Lowest weight is: "+lowest);
    }

    private double getWeight() {
        System.out.println("Enter a weight plz: ");
        return Double.parseDouble(Keyboard.readInput());
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        LowestWeight ex = new LowestWeight();
        ex.start();

    }
}
