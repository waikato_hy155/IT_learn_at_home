package ictgradschool.industry.oop.luckynumbers;

/**
 * Write a program which generates 2 random integers between 25 and 30 (inclusive),
 * then uses Math.min() and Math.max() to display them in descending sequence.
 */
public class LuckyNumbers {
    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private void start() {

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        LuckyNumbers ex = new LuckyNumbers();
        ex.start();

    }
}
