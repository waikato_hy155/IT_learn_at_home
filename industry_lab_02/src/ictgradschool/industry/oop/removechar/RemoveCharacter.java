package ictgradschool.industry.oop.removechar;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a sentence, then prints out the sentence with a random character
 * missing. The program is to be written so that each task is in a separate method. See the comments below for the
 * different methods you have to write.
 */
public class RemoveCharacter {

    private void start() {

        String sentence = getSentenceFromUser();

        int randomPosition = getRandomPosition(sentence);

        printCharacterToBeRemoved(sentence, randomPosition);

        String changedSentence = removeCharacter(sentence, randomPosition);

        printNewSentence(changedSentence);
    }

    /**
     * Gets a sentence from the user.
     * @return
     */
    private String getSentenceFromUser() {

        System.out.println("plz type a sentence");
        String sentence1 = Keyboard.readInput();// TODO Prompt the user to enter a sentence, then get their input.
        return sentence1;
    }

    /**
     * Gets an int corresponding to a random position in the sentence.
     */
    private int getRandomPosition(String sentence) {

        int sentencelengthe = sentence.length();
        int randomPosition = (int) (Math.random()*sentencelengthe+1);
        // TODO Use a combination of Math.random() and sentence.length() to get the desired result.
        return randomPosition;
    }

    /**
     * Prints a message stating the character to be removed, and its position.
     */
    private void printCharacterToBeRemoved(String sentence, int randomPosition) {
        char randomPosition2 =sentence.charAt(randomPosition);
        System.out.println("Removing " +randomPosition2+ " from position " + randomPosition);// TODO Implement this method

    }

    /**
     * Removes a character from the given sentence, and returns the new sentence.
     */
    private String removeCharacter(String sentence,int randomPosition) {
    String x = sentence.substring(0,randomPosition) + sentence.substring(randomPosition+1);
    // TODO Implement this method
        return x;

    }

    /**
     * Prints a message which shows the new sentence after the removal has occured.
     */
    private void printNewSentence(String changedSentence) {

        System.out.println(changedSentence);// TODO Implement this method
    }

    public static void main(String[] args) {
        RemoveCharacter ex = new RemoveCharacter();
        ex.start();
    }
}
