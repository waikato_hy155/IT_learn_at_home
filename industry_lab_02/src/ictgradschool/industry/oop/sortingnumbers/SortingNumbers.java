package ictgradschool.industry.oop.sortingnumbers;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class SortingNumbers {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private void start() {
        System.out.println("lownum?");
        int x = getY();
        System.out.println("highnum?");
        int y = getY();
        int z1 = getZ3(x, y);
        int z2 = getZ3(x, y);
        int z3 = getZ3(x, y);
        System.out.println("sangeshushi:"+z1+","+z2+","+z3);

    }

    private int getY() {
        return Integer.parseInt(Keyboard.readInput());
    }

    private int getZ3(int x, int y) {
        return (int) (Math.random() * (y - x) + 1 + x);
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        SortingNumbers ex = new SortingNumbers();
        ex.start();

    }
}
