package ictgradschool.industry.oop.truncateamount;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter an amount and a number of decimal places.  The program should then
 * truncate the amount to the user-specified number of decimal places using String methods.
 * <p>
 * To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
 * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
 * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
 * You need to write four methods, one method for each of the following tasks:
 * <p>
 * 1. Printing the prompt and reading the amount from the user
 * 2. Printing the prompt and reading the number of decimal places from the user
 * 3. Truncating the amount to the user-specified number of DP's
 * 4. Printing the truncated amount
 */
public class TruncateAmount {

    private void start() {

        String amount = type_a_amount();
        int dp = type_a_DP();
        String bb = dothecut(amount, dp);
        principally(bb);
        // TODO Use other methods you create to implement this program's functionality.
    }

    private String type_a_amount() {
        System.out.println("plz enter a amount here: ");
        return Keyboard.readInput();
    }
    // TODO Write a method which prompts the user and reads the amount to truncate from the Keyboard

    private int type_a_DP() {
        System.out.println("plz enter a DP here: ");
        return Integer.parseInt(Keyboard.readInput());

    } // TODO Write a method which prompts the user and reads the number of DP's from the Keyboard

    private String dothecut(String amount, int dp) {
        int z = amount.indexOf(".");
        return amount.substring(0, z + dp + 1);

    }// TODO Write a method which truncates the specified number to the specified number of DP's

    private void principally(String bb) {
        System.out.println("bb = " + bb);
    // TODO Write a method which prints the truncated amount
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        TruncateAmount ex = new TruncateAmount();
        ex.start();
    }
}
